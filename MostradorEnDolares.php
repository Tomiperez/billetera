<?php

class MostradorEnDolares implements Mostrador {
  public function mostrar(Array $billetes) {
    $total = 0;
    $dolar= 40;
    foreach ($billetes as $billete => $cantidad) {
      $total = $total + $billete*$cantidad;
    }
    return $total/$dolar;
  }
}
